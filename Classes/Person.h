//
// Created by mmm-tasty on 5/27/20.
//

#ifndef VIRUS_SIMULATION_PERSON_H
#define VIRUS_SIMULATION_PERSON_H
#include "VirusState.h"
#include <vector>
#include <functional>

class Person {
public:
    Person(const std::vector<std::reference_wrapper<Person>>& links, const InfectedState& state);

    std::vector<std::reference_wrapper<Person>> links;
    InfectedState state;
};

#endif //VIRUS_SIMULATION_PERSON_H
