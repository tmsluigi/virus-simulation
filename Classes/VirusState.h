//
// Created by mmm-tasty on 5/27/20.
//

#ifndef VIRUS_SIMULATION_VIRUS_STATE_H
#define VIRUS_SIMULATION_VIRUS_STATE_H
enum class InfectedState {
    Seceptible,
    Contagious,
    Immune
};
#endif //VIRUS_SIMULATION_VIRUS_STATE_H
